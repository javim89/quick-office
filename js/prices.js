// precios
let recepcionCorrespondenciaPrecio = 1020;
let oficinaVirtualPrecio = 1530;
let oficinaVirtualFullPrecio = 2000;
let oficinaTemporalHoraSinOVPrecio = 430;
let oficinaTemporalHoraCONOVPrecio = 280;
let oficinaTemporalMediaJornadaSinOVPrecio = 1430;
let oficinaTemporalMediaJornadaCONOVPrecio = 1030;
let oficinaTemporalJornadaSinOVPrecio = 2620;
let oficinaTemporalJornadaCONOVPrecio = 2070;
let reunionesHoraSinOVPrecio = 535;
let reunionesHoraCONOVPrecio = 380;
let reunionesMediaJornadaSinOVPrecio = 2010;
let reunionesMediaJornadaCONOVPrecio = 1340;
let reunionesJornadaSinOVPrecio = 3160;
let reunionesJornadaCONOVPrecio = 2580;
let pFijo = 0;
let pVariable = 0; //reuniones o oficina de reunion

//elementos del DOM
let recepcionCorrespondencia = $("#recepcionCorrespondencia");
let oficinaVirtual = $("#oficinaVirtual");
let oficinaVirtualFull = $("#oficinaVirtualFull");
let oficinaTemporalHora = $("#oficinaTemporalHora");
let oficinaTemporalMediaJornada = $("#oficinaTemporalMediaJornada");
let oficinaTemporalJornada = $("#oficinaTemporalJornada");
let reunionesHora = $("#reunionesHora");
let reunionesMediaJornada = $("#reunionesMediaJornada");
let reunionesJornada = $("#reunionesJornada");
let precioFijo = $("#precioFijo");
let precioVariable = $("#precioVariable");

//elementos variables checkeados
let oficinaChecked;
let reunionChecked;

recepcionCorrespondencia.click(function(){
  if($(this).prop("checked")){
    pFijo = recepcionCorrespondenciaPrecio;
  }else{
    pFijo = 0;
  }
  precioFijo.html(pFijo);
  precioVariable.html(pVariable);
})

oficinaVirtual.click(function(){
  if($(this).prop("checked")){
    recepcionCorrespondencia.prop("checked", true);
    recepcionCorrespondencia.prop("disabled", true);
    pFijo = oficinaVirtualPrecio;
    if(oficinaChecked || reunionChecked){
      pVariable = eval(oficinaChecked.attr("id")+"CONOVPrecio");
      //pVariable = eval(reunionChecked.attr("id")+"CONOVPrecio");
      precioVariable.html(pVariable);
    }
  }else{
    if(oficinaChecked || reunionChecked){
      pVariable = eval(oficinaChecked.attr("id")+"SinOVPrecio");
      //pVariable = eval(reunionChecked.attr("id")+"SinOVPrecio");
      precioVariable.html(pVariable);
    }
    recepcionCorrespondencia.prop("disabled", false);
    pFijo = recepcionCorrespondenciaPrecio;
  }
  precioFijo.html(pFijo);
})

oficinaVirtualFull.click(function(){
  if($(this).prop("checked")){
    recepcionCorrespondencia.prop("checked", true);
    recepcionCorrespondencia.prop("disabled", true);
    oficinaVirtual.prop("checked", true);
    oficinaVirtual.prop("disabled", true);
    pFijo = oficinaVirtualFullPrecio;
  }else{
    oficinaVirtual.prop("disabled", false);
    pFijo = oficinaVirtualPrecio;
  }
  precioFijo.html(pFijo);
  precioVariable.html(pVariable);
})

oficinaTemporalHora.click(function(){
  if($(this).prop("checked")){
    oficinaTemporalMediaJornada.prop("checked", false);
    oficinaTemporalJornada.prop("checked", false);
    oficinaChecked = $(this);
    if(oficinaVirtual.prop("checked") || oficinaVirtualFull.prop("checked")){
        pVariable = oficinaTemporalHoraCONOVPrecio;
    }else{
        pVariable = oficinaTemporalHoraSinOVPrecio;
    }
  }else{
    pVariable = 0;
    oficinaChecked = null;
  }
  precioVariable.html(pVariable);
})

oficinaTemporalMediaJornada.click(function(){
  if($(this).prop("checked")){
    oficinaTemporalHora.prop("checked", false);
    oficinaTemporalJornada.prop("checked", false);
    oficinaChecked = $(this);
    if(oficinaVirtual.prop("checked") || oficinaVirtualFull.prop("checked")){
        pVariable = oficinaTemporalMediaJornadaCONOVPrecio;
    }else{
        pVariable = oficinaTemporalMediaJornadaSinOVPrecio;
    }
  }else{
    pVariable = 0;
    oficinaChecked = null;
  }
  precioVariable.html(pVariable);
})

oficinaTemporalJornada.click(function(){
  if($(this).prop("checked")){
    oficinaTemporalHora.prop("checked", false);
    oficinaTemporalMediaJornada.prop("checked", false);
    oficinaChecked = $(this);
    if(oficinaVirtual.prop("checked") || oficinaVirtualFull.prop("checked")){
        pVariable = oficinaTemporalJornadaCONOVPrecio;
    }else{
      pVariable = oficinaTemporalJornadaSinOVPrecio;
    }
  }else{
    oficinaChecked = null;
    pVariable = 0;
  }
  precioVariable.html(pVariable);
})


reunionesHora.click(function(){
  if($(this).prop("checked")){
    reunionesMediaJornada.prop("checked", false);
    reunionesJornada.prop("checked", false);
    if(oficinaVirtual.prop("checked") || oficinaVirtualFull.prop("checked")){
        precioVariable = reunionesHoraCONOVPrecio;
    }else{
      precioVariable = reunionesHoraSinOVPrecio;
    }
  }else{
    recepcionCorrespondencia.prop("disabled", false);
    oficinaVirtual.prop("disabled", false);
    precioFijo = oficinaVirtualPrecio;
  }
  precio.html(precioVariable);
})

reunionesMediaJornada.click(function(){
  if($(this).prop("checked")){
    reunionesHora.prop("checked", false);
    reunionesJornada.prop("checked", false);
    if(oficinaVirtual.prop("checked") || oficinaVirtualFull.prop("checked")){
        precioVariable = reunionesMediaJornadaCONOVPrecio;
    }else{
      precioVariable = reunionesMediaJornadaSinOVPrecio;
    }
  }else{
    recepcionCorrespondencia.prop("disabled", false);
    oficinaVirtual.prop("disabled", false);
    precioFijo = oficinaVirtualPrecio;
  }
  precio.html(precioVariable);
})

reunionesJornada.click(function(){
  if($(this).prop("checked")){
    reunionesHora.prop("checked", false);
    reunionesMediaJornada.prop("checked", false);
    if(oficinaVirtual.prop("checked") || oficinaVirtualFull.prop("checked")){
        precioVariable = reunionesJornadaCONOVPrecio;
    }else{
      precioVariable = reunionesJornadaSinOVPrecio;
    }
  }else{
    recepcionCorrespondencia.prop("disabled", false);
    oficinaVirtual.prop("disabled", false);
    precioFijo = oficinaVirtualPrecio;
  }
  precio.html(precioVariable);
})
